FROM golang:1.16 as builder

ARG ARG_GOPROXY
ENV GOPROXY $ARG_GOPROXY

WORKDIR /home/app
COPY go.mod go.sum ./

RUN go mod download

COPY . .
RUN go build -o bin/micro-demo 


FROM quay.io/orvice/go-runtime:latest

ENV PROJECT_NAME micro-demo

COPY --from=builder /home/app/bin/${PROJECT_NAME} .
