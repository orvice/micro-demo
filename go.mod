module gitlab.com/orvice/micro-demo

go 1.16

require (
	github.com/golang/protobuf v1.5.2
	github.com/micro/go-micro v1.18.0
	gitlab.com/gitlab-org/project-templates/go-micro v0.0.0-20190225134054-1385fab987bc
)
